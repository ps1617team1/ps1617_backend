'use strict';

const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');

var transporter = nodemailer.createTransport(smtpTransport({
  // host: 'smtp.gmail.com',
  // port: 465,
  // secure: true, // secure:true for port 465, secure:false for port 587
  service: 'gmail',
  auth: {
    user: 'ps1617team1@gmail.com',
    pass: 'cdehmn123456'
  }
}));

// setup email data with unicode symbols
const passwordRecoveryOptions = {
  from: '"Ask.it Support" <no-reply@askit.com>', // sender address
  to: '', // list of receivers
  subject: 'Ask.it Password Recovery', // Subject line
  text: 'Hello, your new password is ', // plain text body
  html: '<b>Hello,</b><br/>your new password is:' // html body
};

exports.sendPasswordRecovery = function(user) {
  let options = JSON.parse(JSON.stringify(passwordRecoveryOptions));
  options.to = user.email;
  options.text += user.password;
  options.html += '<p>' + user.password + '</p>';
  transporter.sendMail(options, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message %s sent: %s', info.messageId, info.response);
  });  
};

// send mail with defined transport object

