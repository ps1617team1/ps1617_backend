'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AnswerSchema = new Schema({
  text: { type: String, required: true },
  post: { type: Schema.Types.ObjectId, ref: 'Post' },
  votes: [{ type: Schema.Types.ObjectId, ref: 'User' }]  
});

module.exports = mongoose.model('Answer', AnswerSchema);