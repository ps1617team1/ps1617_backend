'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  username: { type: String, required: true, unique: true },
  avatar: { type: String, default:'avatar1.jpg' },
  password: { type: String, required: true },
  admin: { type: Boolean, default: false },
  created_at: { type: Date, default: Date.now() },
  updated_at: { type: Date, default: Date.now() },
  friends: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  friend_requests_received: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  friend_requests_sent: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  ignoring: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  answered: [{ type: Schema.Types.ObjectId, ref: 'Answer' }],
  denounced_users: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  denounced_post: [{ type: Schema.Types.ObjectId, ref: 'Post' }],
  denounces: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  my_posts: [{ type: Schema.Types.ObjectId, ref: 'Post' }],
  blocked: { type: Boolean, default: false, required: true }
});

UserSchema.statics.getNameEmail = function( user ) {
  return {name:user.name, email:user.email};
};

UserSchema.statics.filterDataToFrontend = function( user ) {
  delete user.password;
  delete user.__v;
  return user;
};

module.exports = mongoose.model('User', UserSchema);