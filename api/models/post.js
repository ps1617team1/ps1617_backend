'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostSchema = new Schema({
  imagePath: { type: String, required:true },
  creator: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  question: { type: String, required: true },
  answers: [{ type: Schema.Types.ObjectId, ref: 'Answer' }],
  denounces: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  expiration: { type: Date, required: true },
  creation_date: { type: Date, default: Date.now },
  public_level: { type: Number, required: true },
  tag: { type: String, required: true },
  views: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  deleted: { type: Boolean, default: false, required: true }
});

module.exports = mongoose.model('Post', PostSchema);