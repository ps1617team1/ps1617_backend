'use strict';
module.exports.requireAuth = function(req, res, next) {
  if (!req.user) {
    res.json(
      {
        success: false,
        error: {
          toast: true,
          message: 'Authentication required'
        }
      }
    );
  } else {
    next();
  }
};

module.exports.requireAdminAuth = function(req, res, next) {
  if (!req.user) {
    res.json({success: false, code: 'AUTHENTICATION_FALSE'});
  } else {
    if(!req.user.admin){
      res.json({success: false, code: 'AUTHENTICATION_FALSE'});
    }
    else{
      next();
    }
  }
};