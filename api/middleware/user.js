'use strict';
var generator = require('generate-password');

exports.register = function(req, res, next) {
  req.checkBody({
    'username': {
      notEmpty: true,
      matches: {
        options: [/^[a-zA-Z0-9\-_]{0,40}$/],
        errorMessage: 'Invalid username'
      },
      errorMessage: 'Username is required'
    },
    'email': {
      notEmpty: true,
      isEmail: {
        errorMessage: 'Invalid Email Address'
      },
      errorMessage: 'Email is required'
    },
    'password': {
      notEmpty: true,
      errorMessage: 'Password is required'
    }
  });

  var errors = req.validationErrors(true);
  
  if(errors) {
    var keys = Object.keys(errors);
    var values = [];
    for (var i = 0; i < keys.length; i++) {
      values.push(errors[keys[i]]);
    }
    var json = {
      success: false,
      error: {
        fields: values,
        code: 'INVALID_FIELDS',
        message: null,
        toast: true
      }
    };
    return res.json(json);
  }

  req.asyncValidationErrors().catch(function(errors) {
    if(errors) {
      return res.json({
        success: false,
        error: errors
      });
    }
  });
  next();
};

exports.recoverPassword = function(req, res, next) {
  var password = generator.generate({
    length: 10,
    numbers: true
  });
  req.body.password = password;
  next();
};