'use strict';
var posts = require('../controllers/post.js');
var requireAuth = require('../middleware/authentication.js').requireAuth;

module.exports = function(app) {

  app.route('/posts/create')
    .post(requireAuth, posts.create);

  app.route('/posts')
    .get(requireAuth, posts.list);

  app.route('/posts/:post_id')
    .get(requireAuth, posts.read);

  // app.route('/posts/recent')
  //   .get(posts.recent);

  app.route('/posts/report/:post_id')
    .post(requireAuth, posts.report);
    
  app.route('/posts/search/:search_text')
    .get(requireAuth, posts.search);

};