'use strict';
var user = require('../controllers/user.js');
var admin = require('../controllers/admin.js');
var requireAdmin = require('../middleware/authentication.js').requireAdminAuth;

module.exports = function(app) {
  
  app.route('/admin/users/reports')
    .get(requireAdmin, admin.reportedUsers);

  app.route('/admin/users/block')
    .post(requireAdmin, admin.blockUser);

  app.route('/admin/users/rejectreport')
    .post(requireAdmin, admin.rejectReport);

  app.route('/admin/users/blocked')
    .get(requireAdmin, admin.blockedUsers);

  app.route('/admin/users/unblock')
    .post(requireAdmin, admin.unblockUser);
  
    
  app.route('/admin/posts/reports')
    .get(requireAdmin, admin.reportedPosts);

  app.route('/admin/posts/remove')
    .post(requireAdmin, admin.removePost);

  app.route('/admin/posts/rejectreport')
    .post(requireAdmin, admin.removeReports);
};