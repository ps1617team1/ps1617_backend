'use strict';
var friends = require('../controllers/friend.js');
var requireAuth = require('../middleware/authentication.js').requireAuth;

module.exports = function(app) {

  app.route('/friends')
    .get(requireAuth, friends.list_friends);

  app.route('/friends/requests')
    .get(requireAuth, friends.list_friend_requests);

  app.route('/friends/sendrequest')
    .post(requireAuth, friends.send_request);

  app.route('/friends/acceptrequest')
    .post(requireAuth, friends.accept_request);

  app.route('/friends/rejectrequest')
    .post(requireAuth, friends.reject_request);

  app.route('/friends/remove')
    .delete(requireAuth, friends.remove_friend);

};