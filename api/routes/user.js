'use strict';
var user = require('../controllers/user.js');
var middleware = require('../middleware/user.js');
var requireAuth = require('../middleware/authentication.js').requireAuth;

module.exports = function(app) {
  
  app.route('/users')
    .get(user.list);
    
  app.route('/users/register')
    .post(middleware.register, user.register);

  app.route('/users/login')
    .post(user.loginLocal);

  app.route('/users/logout')
    .delete(user.logout);

  app.route('/users/recoverpassword')
    .post(middleware.recoverPassword, user.recoverPassword),

  app.route('/users/:userId')
    .get(user.read)
    .put(user.update)
    .delete(user.delete);

  app.route('/users/:userId/posts')
    .get(user.postsByUser);

  app.route('/users/search/:search_text')
    .get(requireAuth, user.search);

  app.route('/users/report/:user_id')
    .post(requireAuth, user.report);
};