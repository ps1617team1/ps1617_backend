'use strict';
var answers = require('../controllers/answers.js');
var requireAuth = require('../middleware/authentication.js').requireAuth;

module.exports = function(app) {

  app.route('/votes/:answer_id')
    .post(requireAuth, answers.vote);

};