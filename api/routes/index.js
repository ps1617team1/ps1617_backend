module.exports = function(app) {
  require('./user.js')(app);
  require('./post.js')(app);
  require('./friend.js')(app);
  require('./votes.js')(app);
  require('./admin.js')(app);
};