'use strict';
var User = require('mongoose').model('User');

exports.list_friends = function(req, res) {
  User.find({_id:req.user._id})
    .populate('friends')
    .exec(function(err, user){
      var friends = user[0].friends;
      /*for( var i=0; i<friends.length ; i++ ){
        friends[i] = User.filterDataToFrontend(friends[i]);
      }*/
      
      res.json({list : friends});
    });
};

exports.list_friend_requests = function(req, res) {
  res.json({list : req.user.friend_requests_received});
};

exports.send_request = function(req, res) {
  if (req.body.user_id) {
    User.findByIdAndUpdate(
      {_id: req.user._id},
      {$addToSet: {friend_requests_sent: req.body.user_id}},
      function(err) {
        if (err) {
          return res.json(err);
        }
      });
    User.findByIdAndUpdate(
      {_id: req.body.user_id},
      {$addToSet: {friend_requests_received: req.user._id}}, {new: true},
      function(err, user) {
        if (err) {
          res.json(err);
        } else {
          res.json(user);
        }
      });
  }
};

exports.accept_request = function(req, res) {
  // check if body was well sent
  if (!req.body.user_id){
    return res.json( {success: false,error: {toast: true, message: 'Incorrect UserId'} } );
  }

  // check if friend_requests_received contains req.body.user_id 
  var isUserOnList = false;
  req.user.friend_requests_received.forEach(function(element) {
    if( element == req.body.user_id ){
      isUserOnList = true;
    }
  }, this);
  
  if( !isUserOnList ){
    return res.json( {success: false,error: {toast: true, message: 'Selected UserId is not on your friend_requests_received'} } );
  }

  // update "my" data 
  User.findByIdAndUpdate( 
    {_id: req.user._id}, 
    {
      $addToSet: {friends: req.body.user_id},
      $pull: {friend_requests_received: req.body.user_id}
    },
    function(err) {
      if (err) {
        return res.json(err);
      }
      // update "accepted friend" data
      User.findByIdAndUpdate(
        {_id: req.body.user_id},
        {
          $addToSet: {friends: req.user._id},
          $pull: {friend_requests_sent: req.user._id}
        },
        function(err, usr) {
          if (err) {
            return res.json(err);
          }
          res.json({success: true, message: usr.name+ ' friend request was accepted'});
        }
      );
    }
  );

          
        
              
};

module.exports.reject_request = function(req, res) {
  // check if body was well sent
  if (!req.body.user_id){
    return res.json( {success: false,error: {toast: true, message: 'Incorrect UserId'} } );
  }

  // check if friend_requests_received contains req.body.user_id 
  var isUserOnList = false;
  req.user.friend_requests_received.forEach(function(element) {
    if( element == req.body.user_id ){
      isUserOnList = true;
    }
  }, this);
  
  if( !isUserOnList ){
    return res.json( {success: false,error: {toast: true, message: 'Selected UserId is not on your friend_requests_received'} } );
  }

  // update "my" data 
  User.findByIdAndUpdate( {_id: req.user._id}, {$pull: {friend_requests_received: req.body.user_id}},
    function(err) {
      if (err) {
        return res.json(err);
      }
      // update "rejected friend" data
      User.findByIdAndUpdate(
        {_id: req.body.user_id},
        {$pull: {friend_requests_sent: req.user._id}},
        function(err, usr) {
          if (err) {
            return res.json(err);
          }
          res.json({success: true, message: usr.name+' friend request was removed'});
        }
      );
    }
  );
};


module.exports.remove_friend = function(req, res) {
  if (!req.body.user_id){
    return res.json( {success: false,error: {toast: true, message: 'Incorrect UserId'} } );
  }
  
  // check if friends contains req.body.user_id 
  var isUserOnList = false;
  req.user.friends.forEach(function(element) {
    if( element == req.body.user_id ){
      isUserOnList = true;
    }
  }, this);
  
  if( !isUserOnList ){
    return res.json( {success: false,error: {toast: true, message: 'Selected UserId is not your friend'} } );
  }

  // update "my" data 
  User.findByIdAndUpdate( 
    {_id: req.user._id}, 
    {
      $pull: {friends: req.body.user_id}
    },
    function(err) {
      if (err) {
        return res.json(err);
      }
      // update "accepted friend" data
      User.findByIdAndUpdate(
        {_id: req.body.user_id},
        {
          $pull: {friends: req.user._id}
        },
        function(err, usr) {
          if (err) {
            return res.json(err);
          }
          res.json({success: true, message: usr.name+' friend removed'});
        }
      );
    }
  );
};