'use strict';
var User = require('mongoose').model('User');
var nodemailer = require('../external/nodemailer.js');
var passport = require('passport');
var Post = require('mongoose').model('Post');

exports.list = function(req, res) {
  User.find({}, function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.register = function(req, res) {
  var new_user = new User(req.body);
  new_user.save(function(err, user) {
    if (err)
      res.json(err);
    res.json(user);
  });
};

exports.loginLocal = [
  passport.authenticate('local'), 
  function(req,res) {
    res.json({user_info : req.user , session_token : req.session.passport.user});
  }
];

exports.logout = function(req, res) {
  req.logout();
  res.sendStatus(200);
};

exports.read = function(req, res) {
  User.findById(req.params.userId)
    .populate('my_posts')
    .populate('denounces')
    .populate('denounced_post')
    .populate('denounced_users')
    .populate('answered')
    .populate({
      path: 'answered',			
      populate: {
        path:  'post',
        model: 'Post'
      }
    })
    .populate('ignoring')
    .populate('friend_requests_sent')
    .populate('friend_requests_received')
    .populate('friends')
    .exec(function(err, user) {
      if (err)
        res.send(err);
      res.json(user);
    });
};

exports.update = function(req, res) {
  User.findOneAndUpdate(req.params.userId, req.body, {new: true}, function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.delete = function(req, res) {
  User.remove({
    _id: req.params.userId
  }, function(err) {
    if (err)
      res.send(err);
    res.json({ message: 'user successfully deleted' });
  });
};

exports.recoverPassword = function(req, res) {
  User.findOneAndUpdate(req.body.email, { password: req.body.password }, {new: true}, function(err, user) {
    if (err) {
      res.send(err);
    }
    nodemailer.sendPasswordRecovery(user);
    res.json(user);
  });
};

exports.postsByUser = function(req, res){
  var userId = req.params.userId;
  if(!userId){
    console.log({message:'Invalid userId'});
    res.json({message:'Invalid userId'});
  }
  else{
    Post.find({creator:userId})
    .populate('creator')
    .populate('answers')
    .populate('answers.votes')
    .exec(function(err, posts) {
      if (err) {
        console.log(err);
        res.json(err);
      } else {
        console.log(JSON.stringify(posts, null, '\t'));
        res.json(posts);
      }
    });
  }
};

exports.search = function(req, res) {
  var query = req.params.search_text.replace(/_/g, ' ');
  User.find({name: new RegExp(query, 'i')}, function(err, users) {
    if (err)
      res.send(err);
    res.json(users);
  });
};

exports.report = function(req, res) {
  User.findByIdAndUpdate(req.params.user_id, {$addToSet: { denounces: req.user._id }}, {new: true} , function(err) {
    if (err) {
      res.json(err);
    }
  });
  User.findByIdAndUpdate(req.user._id, {$addToSet: { denounced_users: req.params.user_id }}, {new: true} , function(err, user) {
    if (err) {
      res.json(err);
    } else {
      res.json(user);
    }
  });
};
