'use strict';
var User = require('mongoose').model('User');
var Post = require('mongoose').model('Post');

exports.reportedUsers = function(req, res) {
  User.find({ denounces: { $not: {$size:0} }, blocked: {$in: [null, false]} })
    .populate('my_posts')
    .populate('denounces')
    .populate('denounced_post')
    .populate('denounced_users')
    .populate('answered')
    .populate({
      path: 'answered',			
      populate: {
        path:  'post',
        model: 'Post'
      }
    })
    .populate('ignoring')
    .populate('friend_requests_sent')
    .populate('friend_requests_received')
    .populate('friends')
    .exec(function(err, users) {
      if (err)
        res.send(err);
      res.json(users);
    });
};
exports.reportedPosts = function(req, res) {
  Post.find({ denounces: { $not: {$size:0} }, deleted: {$in: [null, false]} })
    .populate('creator')
    .populate('answers')
    .populate('answers.votes')
    .exec(function(err, posts) {
      if (err)
        res.send(err);
      res.json(posts);
    });
};

exports.rejectReport = function(req, res) {
  User.findByIdAndUpdate(req.body.user_id, {$set: { denounces: [] }}, {new: true} , function(err, user) {
    if (err) {
      res.json(err);
    } else {
      res.json(user);
    }
  });
};

exports.blockedUsers = function(req, res) {
  User.find({ blocked: true })
    .populate('my_posts')
    .populate('denounces')
    .populate('denounced_post')
    .populate('denounced_users')
    .populate('answered')
    .populate({
      path: 'answered',			
      populate: {
        path:  'post',
        model: 'Post'
      }
    })
    .populate('ignoring')
    .populate('friend_requests_sent')
    .populate('friend_requests_received')
    .populate('friends')
    .exec(function(err, users) {
      if (err)
        res.send(err);
      res.json(users);
    });
};

exports.blockUser = function(req, res) {
  User.findByIdAndUpdate(req.body.user_id, {$set: { blocked: true }}, {new: true} , function(err, user) {
    if (err) {
      res.json(err);
    } else {
      res.json(user);
    }
  });
};

exports.unblockUser = function(req, res) {
  User.findByIdAndUpdate(req.body.user_id, {$set: { blocked: false, denounces: [] }}, {new: true} , function(err, user) {
    if (err) {
      res.json(err);
    } else {
      res.json(user);
    }
  });
};

exports.rejectReport = function(req, res) {
  User.findByIdAndUpdate(req.body.user_id, {$set: { denounces: [] }}, {new: true} , function(err, user) {
    if (err) {
      res.json(err);
    } else {
      res.json(user);
    }
  });
};

exports.removePost = function(req, res) {
  Post.findByIdAndUpdate(req.body.post_id, {$set: { deleted: true }}, {new: true} , function(err, post) {
    if (err) {
      res.json(err);
    } else {
      res.json(post);
    }
  });
};

exports.removeReports = function(req, res) {
  Post.findByIdAndUpdate(req.body.post_id, {$set: { denounces: [] }}, {new: true} , function(err, post) {
    if (err) {
      res.json(err);
    } else {
      res.json(post);
    }
  });
};