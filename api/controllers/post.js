'use strict';
var Post = require('mongoose').model('Post');

var Answer = require('mongoose').model('Answer');
var User = require('mongoose').model('User');
var dropbox = require('../../config/dropbox.js');

exports.create = function(req, res) {
  var errBody = _validateFieldsCreatePost(req.body);
  if(errBody) {
    res.json(errBody);
    return;
  } 

  console.log(JSON.stringify(req.body.expiration));
  req.body.expiration = new Date(parseInt(req.body.expiration));  
  console.log(JSON.stringify(req.body.expiration));

  var new_post = new Post(req.body);
  new_post.creator = req.user._id;
  delete new_post.base64;

  var image = dropbox.decodeBase64Image(req.body.base64);
  new_post.imagePath = '/images/'+req.user._id +'/'+new_post._id+'.'+image.type;
  dropbox.filesUpload({path: new_post.imagePath, contents:image.data})
    .then(function(response) {
      dropbox.sharingCreateSharedLinkWithSettings({path:new_post.imagePath})
        .then(function (data) {
          data.url += '&raw=1';
          new_post.imagePath = data.url;

          var answers = [];
          for (var i = 0; i < req.body.answers.length; i++) {
            var answerData = {
              text: req.body.answers[i],
              post: new_post._id,
              votes: []
            };
            var new_answer = new Answer(answerData);
            answers.push(new_answer._id);
            new_answer.save();
          }
          new_post.answers = answers;
          new_post.save(function(err, post) {
            if (err) {
              console.error({success:false,message:err});
              res.json({success:false,message:err});
            } else {
              console.error({success:true,message:post});
              res.json({success:true,message:post});
            }
          });
        })
        .catch(function (err) {
          console.error(err);
          res.json({success:false,message:err});
        });
    })
    .catch(function(error) {
      console.error(error);
      res.json({success:false,message:error});
  });
};

function _validateFieldsCreatePost(body){
  if( !body.question )    return {success:false, message:'A pergunta é obrigatória'};
  if( !body.base64 )      return {success:false, message:'A imagem é obrigatória'};
  if( !body.expiration )  return {success:false, message:'A data de expiração é obrigatória'};
  if( !body.public_level )return {success:false, message:'O nivel de privacidade é obrigatória'};

  var expiration = new Date(parseInt(body.expiration));
  var today = new Date();
  if( today > expiration )  return {success:false, message:'A data de expiração não pode ser no passado'};

  return null;
}

exports.list = function(req, res) {
  Post.find({ deleted: {$in: [null, false]} }).sort('-creation_date')
    .populate('creator')
    .populate('answers')
    .populate('answers.votes')
    .exec(function(err, posts) {
      if (err) {
        res.json(err);
      } else {
        res.json(posts);
      }
    });
};

exports.read = function(req, res) {
  Post.findByIdAndUpdate(req.params.post_id, {$addToSet: { views: req.user._id }}, {new: true}, function(err) {
    if (err) {
      return res.json(err);
    }
  });
  Post.findById(req.params.post_id)
    .populate('creator')
    .populate('answers')
    .populate('answers.votes')
    .exec(function(err, post) {
      if (err) {
        res.json(err);
      } else {
        res.json(post);
      }
    });
};

exports.report = function(req, res) {
  User.findByIdAndUpdate(req.user._id, {$addToSet: { denounced_post: req.params.post_id }}, {new: true} , function(err, user) {
    console.log(user);
    if (err) {
      return res.json(err);
    }
  });
  Post.findByIdAndUpdate(req.params.post_id, {$addToSet: { denounces: req.user._id }}, {new: true} , function(err, post) {
    if (err) {
      res.json(err);
    } else {
      res.json(post);
    }
  });
};

exports.search = function(req, res) {
  var query = req.params.search_text.replace(/_/g, ' ');
  Post.find({question: new RegExp(query, 'i')})
    .populate('creator')
    .populate('answers')
    .populate('answers.votes')
    .exec(function(err, posts) {
      if (err)
        res.send(err);
      res.json(posts);
    });
};