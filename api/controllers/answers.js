'use strict';
var Answer = require('mongoose').model('Answer');
var Post = require('mongoose').model('Post');
var User = require('mongoose').model('User');

exports.vote = function(req, res) {
  Answer.findByIdAndUpdate(req.params.answer_id, {$addToSet: { votes: req.user._id }}, {new: true})
    .populate('post')
    .exec(function(err, answer) {
      var error = null;
      if (err) {
        console.log(JSON.stringify(err));
        res.json({success:false, message:err});
        return;
      } else {
        User.findByIdAndUpdate(req.user._id, {$addToSet: { answered: req.params.answer_id }}, {new: true}, function(err) {
          if (err) {
            return res.json(err);
          }
        });
        var post = answer.post;
        post.answers.forEach(function(ansr) {
          if (ansr != req.params.answer_id) {
            User.findByIdAndUpdate(req.user._id, {$pull: { answered: ansr }}, {new: true}, function(err) {
              if (err) {
                console.log(JSON.stringify(err));
                res.json({success:false, message:err});
                return;
              }
            });
            Answer.findByIdAndUpdate(
              ansr,
              {$pull: { votes: req.user._id }}, {new: true},
              function(err) {
                if (err) {
                  console.log(JSON.stringify(err));
                  error = err;
                }
              }
            );
          }
        }, this);
      }
      if (error) {
        console.log(JSON.stringify(error));
        res.json(error);
      } else {
        Post.findById(post._id)
          .populate('answers')
          .exec(function(err, post) {
            if (err){
              res.send(err);
              console.log(JSON.stringify(err));
              return;
            }
            console.log({success:true , message:"Answer successfully saved"});
            res.json( {success:true , message:"Answer successfully saved"} );
          }
        );
      }
    });
};
