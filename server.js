var port = process.env.PORT || 3030,
  mongoose = require('./config/mongoose'),
  app = require('./config/app'),
  passport = require('./config/passport');

var db = mongoose();
app = app(db);
passport();

app.listen(port);

console.log('Ask.it running on port: ' + port);