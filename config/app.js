// Load the module dependencies
var express = require('express'),
  bodyParser = require('body-parser'),
  session = require('express-session'),
  MongoStore = require('connect-mongo')(session),
  passport = require('passport');
var expressValidator = require('express-validator');

module.exports = function(db){

  var app = express();
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(expressValidator());

  // Configure log middleware
  app.use(function(req, res, next) {
    console.log('Request: ' + req.method + ' ' + req.url);
    next();
  });
  
  // Configure 'session' middleware
  app.use(session({
    saveUninitialized: true,
    resave: true,
    secret: 'sEcretpassWord19571042',
    store: new MongoStore({mongooseConnection: db.connection})
  }));

  // passport middleware
  app.use(passport.initialize());
  app.use(passport.session());

  // middleware to test sessions (can be deleted later)
  app.use(function(req, res, next) {
    if( req.user )
      console.log('Request by : ' + req.user.name);
    next();
  });

  // Load routes
  require('../api/routes')(app);
  // Load dropbox auth
  require('./dropbox');
  // Configure static file serving
  app.use(express.static('./public'));

  return app;
};