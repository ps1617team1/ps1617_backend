var mongoose = require('mongoose');

module.exports = function(){
  //mongoose.Promise = global.Promise;
  var db;
  if (process.env.PRODUCTION) {
    db = mongoose.connect(process.env.MONGODB_URI);
  } else {
    db = mongoose.connect('mongodb://localhost/ask_it');
  }

  // Load the application models 
  require('../api/models/user');
  require('../api/models/post');
  require('../api/models/answer');
  return db;
};