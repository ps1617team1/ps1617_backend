'use strict';

var passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  User = require('mongoose').model('User');

module.exports = function() {
  passport.use(new LocalStrategy(function(username, password, done) {
    User.findOne({username: username}, function(err, user) {
      if (err) {
        console.log('[passportlocal] err  -> '+err);
        return done(err);
      }
      
      // if user not found, err
      if (!user) {
        return done(null, false);
      }

      if( "blocked" in user ){
        if( user.blocked ){
          return done(null, false);
        }
      }
      // if passport is incorrect, err
      if (user.password!=password) {
        return done(null, false);
      }
      
      // otherwise, logged in
      console.log('[passportlocal] logged in successfully  -> '+user.name);
      return done(null, user);
    });
  }));
};